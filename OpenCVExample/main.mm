//
//  main.m
//  OpenCVExample
//
//  Created by Artist Planet on 5/21/13.
//  Copyright (c) 2013 Mike Liu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
