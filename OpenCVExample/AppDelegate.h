//
//  AppDelegate.h
//  OpenCVExample
//
//  Created by Artist Planet on 5/21/13.
//  Copyright (c) 2013 Mike Liu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
